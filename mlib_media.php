<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Add Media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Media</h2>
  <form action="mlib_media.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>Title</td>
        <td align="left"><input type="text" name="title" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Author</td>
        <td align="left"><input type="text" name="author" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Description</td>
        <td align="left"><input type="text" name="description" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Type</td>
        <td align="left">
		   <select name="type">
<?php
  //select pull down menu
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display from mlib_types
    $result = $db->query('SELECT * FROM mlib_types');
    foreach($result as $row)
    {
      print "<option value=".$row['type'].">".$row['type']."</option>";
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
	</select>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  # Process the information from the form displayed
  $title = $_POST['title'];
  $author = $_POST['author'];
  $description = $_POST['description'];
  $type = $_POST['type'];

  //clean up data
  $title = trim($title);
  if ( empty($title) ) {
    try_again("Title field is required.");
  }
  $author = trim($author);
  if ( empty($author) ) {
    try_again("Author field must have a name.");
  }
  $description = trim($description);
  if ( empty($description) ) {
    try_again("Description field is required.");
  }
  $type = trim($type);
  if ( empty($type) ) {
    try_again("Type field is required.");
  }
  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check duplicate
    $sql = "SELECT COUNT(*) FROM media WHERE title = '$title' AND status = 'active'";
    $result = $db->query($sql)->fetch(); //count entries w title
    if ( $result[0] > 0) {
      try_again($title." is not unique. Titles must be unique.");
    }

    //insert data
    $db->exec("INSERT INTO media (title, author, description, type, status, user_id) VALUES ('$title', '$author', '$description', '$type', 'active', 0);");

    //get last id val
    $last_id = $db->lastInsertId();

    //now output html table
    print "<h2>Media Entered</h2>";
    print "<table border=1>";
    print "<tr>";
    print "<td>Id</td><td>Title</td><td>Author</td><td>Description</td><td>Type</td><td>Status</td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM media where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
    print "<td>".$row['id']."</td>";
    print "<td>".$row['title']."</td>";
    print "<td>".$row['author']."</td>";
    print "<td>".$row['description']."</td>";
    print "<td>".$row['type']."</td>";
    print "<td>".$row['status']."</td>";
    print "</tr>";
    print "</table>";

    //close
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
