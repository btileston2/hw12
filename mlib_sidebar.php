<aside id="sidebar">
  <nav>
    <h2>Actions</h2>
    <ul>
      <li>
        <a href="mlib_status.php">Media Status</a><br/>
      </li>
      <li>
        <a href="mlib_reserve.php">Reserve Media</a><br/>
      </li>
      <li>
        <a href="mlib_release.php">Release Media</a><br/>
      </li>
      <li>
        <a href="mlib_login.php">Login as Admin</a><br/>
      </li>
<?php
  if (!empty($_SESSION['valid_user'])) {
?>
      <hr/>
      <li>
        <a href="mlib_media.php">Add Media</a><br/>
      </li>
      <li>
        <a href="mlib_users.php">Add User</a><br/>
      </li>
      <li>
        <a href="mlib_upload.php">Upload Media</a><br/>
      </li>
      <li>
        <a href="mlib_administrator.php">Admin Config</a><br/>
      </li>
      <li>
        <a href="mlib_logout.php">Log Out</a><br/>
      </li>
<?php
  }
?>
    </ul>
  </nav>
</aside>
