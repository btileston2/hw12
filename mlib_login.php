<!doctype html>
<?php
session_start();
require('mlib_functions.php');
require('mlib_values.php');
html_head("Login Page");
require('mlib_header.php');


if (isset($_POST['login']) && isset($_POST['password']))
{
  $login = $_POST['login'];
  $password = $_POST['password'];
  $passwd = sha1($password);

  try
  {
    //open database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check for login in the users table
    $sql = "SELECT count(*) FROM mlib_users WHERE login = '$login' AND password = '$passwd'";
    $result = $db->query($sql)->fetch();//count num entree same
    if ( $result[0] == 1) {
      $_SESSION['valid_user'] = $login;
    }

    //close db
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
}

require('mlib_sidebar.php');
//if session var is set then the user has enterd a vaild login/password
if (isset($_SESSION['valid_user'])) {
  echo "You are logged in as: ".$_SESSION['valid_user'].'<br/>';
} else {
  if (isset($login)) {
    echo "Either your login or password is incorrect.".'<br/>';
  } else {
  //the user is coming through for the first time so display a form to accept  l/p
  ?>
  <h2>Login for Administration</h2>
  <form action="mlib_login.php" method="post">
    <table border="0">
      <tr>
        <td bgcolor="#cccccc" width="100">Login</td>
        <td><input type="text" name="login"></td>
      </tr>
      <tr>
        <td bgcolor="#cccccc" width="100">Password</td>
        <td><input type="password" name="password"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Log In"></td>
      </tr>
    </table>
  </form>
<?php
 }
}

require('mlib_footer.php');
?>
